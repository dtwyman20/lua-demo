# lua-repo

Repository for lua files

The following scripts work with lua 5.4.3

Listing

first.lua

  This is a "hello world" in lua

luatut.lua

  The source of this script comes from the YouTube Lua Tutorial from Derek Banas
  <https://www.youtube.com/watch?v=iMacxZQMPXs>
