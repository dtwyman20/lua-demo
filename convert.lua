local metricConvert = {}

function metricConvert.ftToCm(ft)
    return (ft * 30.48)
end
return metricConvert